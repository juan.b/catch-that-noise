import numpy as np
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import EarthLocation, AltAz
from astropy.coordinates import TEME, CartesianDifferential, CartesianRepresentation


def propagate(satellite, starting_date, propagation_time, dT):
    """
    This function propagates satellite using SPG4

    Parameters
    ----------
    satellite: spg4 satellite class
        Satellite orbital information
    starting_date: astropy time class
        Start time for propagation
    propagation_time: float
        Propagation period
    dT: float
        Time step

    Returns
    -------
    teme_p :  nd-array
        Position of satellite in TEME frame [km]
    teme_v :  nd-array
        Velocity of satellite in TEME frame [km/s]
    time :  1D-array
        Discretization time [days]
    """

    t1 = starting_date.jd
    t2 = t1 + propagation_time

    time_steps = np.linspace(t1, t2, int((t2-t1) * 86400/dT))

    jd = time_steps.astype(int)
    fr = time_steps - time_steps.astype(int)

    error_code, teme_p, teme_v = satellite.sgp4_array(jd, fr)  # in km and km/s

    time = Time(time_steps, format='jd')

    return teme_p, teme_v, time


def teme2AzEl(r, v, time, station_lat, station_lon, station_height):

    #transform r and v coordinates into astropy classes
    teme_p = CartesianRepresentation(r.T*u.km)
    teme_v = CartesianDifferential(v.T*u.km/u.s)
    teme = TEME(teme_p.with_differentials(teme_v), obstime=time)

    #montegancedo coordinates
    station = EarthLocation.from_geodetic(station_lon * u.deg, station_lat * u.deg, station_height * u.m)

    result = teme.transform_to(AltAz(obstime=time, location=station))

    return result.az.value, result.alt.value


class TLE():
    def __init__(self, name, line_1, line_2):
        self.name = name
        self.line_1 = line_1
        self.line_2 = line_2

    @property
    def TLE_lines(self):
        return (self.line_1, self.line_2)

    @property
    def id(self):
        return self.line_2[2:7]


def detect_access(elevation_array):
    """
    Detect start and end of access by checking elevation change and
    return the indexes where they happen.

    Parameters
    ----------
    elevation_array :  1D
        Date in ISO format: "2021-01-01 12:00:00.000"
    period :  float
        Extension of time considered to detect accesses (days)

    Returns
    -------
    idx_start :  1D-array
        Indexes of accesses start
    idx_start :  1D-array
        Indexes of accesses end
    """

    #add zero at the start and bottom of the array
    idx_ext = np.concatenate([[0], elevation_array, [0]])

    #detect start and end of an acces
    access_io = np.where(np.sign(idx_ext[:-1]) != np.sign(idx_ext[1:]))[0]

    #extract index of the start
    idx_start = access_io[0::2] - 1
    idx_end = access_io[1::2] - 1

    return idx_start, idx_end


def f_observed(f0, v_rel):
    c = 299792.458 # light speed [km/s]
    f = f0 * (1 + v_rel/c)
    return f


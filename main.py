import numpy as np
from astropy.time import Time
from sgp4.api import Satrec
from tools import propagate, teme2AzEl, TLE

# start and end time of signal reception
# encounter #7054703
times = ("2023-01-21 10:21:41", "2023-01-21 10:28:45")
date0 = Time(times[0], scale='utc')
datef = Time(times[1], scale='utc')

date_a = date0 + (datef-date0)/2

# encounter #7096731
times = ("2023-01-30 10:45:19", "2023-01-30 10:51:21")
date0 = Time(times[0], scale='utc')
datef = Time(times[1], scale='utc')

date_b = date0 + (datef-date0)/2

# Ground station data
lon = -4.273
lat = 40.361
height = 613 #meters
min_elevation = 15 #deg

# read ids
with open('suspects_id.txt') as f:
    ids = [int(line.rstrip('\n')) for line in f].sort()

# read TLEs
with open('suspects_TLE.txt') as f:
    subline_idx = 0
    TLE_components = []
    TLE_list = []
    for line in f:
        if subline_idx == 3:
            TLE_list.append(TLE(*TLE_components))
            TLE_components = []
            subline_idx = 0
        TLE_components.append(line)
        subline_idx +=1

# upmsat data
TLE_upm = TLE('UPMSAT-2',
              "1 46276U 20061E   23029.70090925  .00005763  00000-0  26661-3 0  9997",
              "2 46276  97.3661 100.3230 0002719  93.8395 266.3151 15.20520521133222")


encounter_a = []
print("#7054703")
for TLE_sat in TLE_list:
    satellite = Satrec.twoline2rv(*TLE_sat.TLE_lines)
    r, v, time = propagate(satellite, date_a, 60/86400, 1)
    _, elevation = teme2AzEl(r, v, time, lat, lon, height)
    if np.any(elevation>0):
        print(TLE_sat.id)
        encounter_a.append(TLE_sat.id)
print("\n")


encounter_b = []
print("#7096731")
for TLE_sat in TLE_list:
    satellite = Satrec.twoline2rv(*TLE_sat.TLE_lines)
    r, v, time = propagate(satellite, date_b, 60/86400, 1)
    _, elevation = teme2AzEl(r, v, time, lat, lon, height)
    if np.any(elevation>0):
        print(TLE_sat.id)
        encounter_b.append(TLE_sat.id)
print("\n")
print("Satellites above horizon in both dates")
print(set(encounter_a) & set(encounter_b))
# Who is the noisy satellite nearby UPMSat-2?

Rafael EA4BPN from SatNOGS [catch](https://community.libre.space/t/upmsat-2-object-46276/10074) a signal from another satellite when receiving from UPMSat-2.

This code has led to the identification of UNISAT-7 as the suspected source of the signal.

First, potential suspects have been [identified](https://community.libre.space/t/upmsat-2-object-46276/10074/29) from the SatNOGS DB based on their registered transmission frequency. Looking for a frequency similar to UPMSat-2 (±65.1KHz).

Then, their TLEs have been retrieved from [space-track.org](http://space-track.org/) at the time of the [second observation](https://network.satnogs.org/observations/7096731/).

By looking at the position of all these satellites in both dates, we can easily check which ones are above the horizon from Rafa's ground station perspective. Cross checking the results led to the only two satellites being on the sky at that given time: [46276](https://db.satnogs.org/satellite/UCSJ-1439-6113-9699-8457) and [47945](https://db.satnogs.org/satellite/QKBK-9080-1354-2848-1016). The first one is the UPMSat-2 so our leading suspect is... [UNISAT-7](https://www.gaussteam.com/satellites/gauss-latest-satellites/unisat-7/)!


## Acknowledgments

[Rafa](https://community.libre.space/u/ea4bpn), [Fredy](https://community.libre.space/u/fredy/summary) and Ángel contributed to this brief work, ideal for a friday morning.

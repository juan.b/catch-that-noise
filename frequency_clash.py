import numpy as np
from astropy.time import Time
from sgp4.api import Satrec
from tools import propagate, teme2AzEl, TLE, detect_access, f_observed
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm


# simulation parameters
# dT (if too low, it can slow the computation)
dT = 60
dT_fine = 1

distance_threshold = 2000  # km


# start and end time as string
times = ("2023-01-01 00:00:00", "2023-02-01 00:00:00")
date0 = Time(times[0], scale='utc')
datef = Time(times[1], scale='utc')
tof = (datef - date0).jd

# Ground station data
lon = -4.273
lat = 40.361
height = 613 #meters
min_elevation = 15 #deg

# Frequencies (from SatNOGS)
f0_upmsat = 437.405  # [MHz]
f_margin = 10e-3  # [MHz]

f0_unisat = 437.425  # [MHz]

# TLE from files (from space-track, date = date0)
with open('TLE_files/frequency_clash/UNISAT7.txt') as f:
    TLE_components = ['UNISAT7']

    for line in f:
        TLE_components.append(line)
    unisat_TLE = TLE(*TLE_components)
    unisat = Satrec.twoline2rv(*unisat_TLE.TLE_lines)

with open('TLE_files/frequency_clash/UPMSAT2.txt') as f:
    TLE_components = ['UPMSAT2']

    for line in f:
        TLE_components.append(line)
    upmsat_TLE = TLE(*TLE_components)
    upmsat = Satrec.twoline2rv(*upmsat_TLE.TLE_lines)

print("Coarse propagation...")
# coarse propagation
r_unisat, v_unisat, time_unisat = propagate(unisat, date0, tof, dT)

r_upmsat, v_upmsat, time_upmsat = propagate(upmsat, date0, tof, dT)

# transformation into elevation
_, elevation_unisat = teme2AzEl(r_unisat, v_unisat, time_unisat, lat, lon, height)
_, elevation_upmsat = teme2AzEl(r_upmsat, v_upmsat, time_upmsat, lat, lon, height)

# # Detect accesses
# idx_start_unisat, idx_end_unisat = detect_access(elevation_unisat)
# idx_start_upmsat, idx_end_upmsat = detect_access(elevation_upmsat)



time_coarse = np.copy(time_unisat)
# = (time_unisat - date0).jd

distance = np.linalg.norm((r_unisat - r_upmsat), axis=1)

velocity = np.linalg.norm((v_unisat - v_upmsat), axis=1)
acceleration = np.diff(velocity)
acceleration = np.insert(acceleration, 0, acceleration[0])
acceleration_sign = np.sign(acceleration)


# filter data for encounters over the ground station
print("filtering...")
filter_unisat = distance < distance_threshold
filter_upmsat = distance < distance_threshold

filter_sats = np.logical_and(filter_unisat, filter_upmsat)

idx_start, idx_end = detect_access(filter_sats)

duration_array = 2 * (idx_end - idx_start)*60/86400 + 5/(24*60)

ids = np.arange(len(idx_start))
access_list = []
pbar = tqdm(total=len(idx_start))
for jdx, idx in enumerate(idx_start):
    row_dict = {}

    #Start of fine propagation 5 minutes before access start detected coarsely
    date_access = time_coarse[idx] - 5/(24 * 60)

    #Fine propagation for each access
    # coarse propagation
    r_unisat, v_unisat, time_unisat = propagate(unisat, date_access, duration_array[jdx], dT_fine)

    r_upmsat, v_upmsat, time_upmsat = propagate(upmsat, date_access, duration_array[jdx], dT_fine)

    distance = np.linalg.norm((r_unisat - r_upmsat), axis=1)

    velocity = np.linalg.norm((v_unisat - v_upmsat), axis=1)
    acceleration = np.diff(velocity)
    acceleration = np.insert(acceleration, 0, acceleration[0])
    acceleration_sign = np.sign(acceleration)

    #index of start and end of access
    idx_start_fine, idx_end_fine = detect_access(distance < distance_threshold)

    start_date = (time_unisat[idx_start_fine][0])
    end_date = (time_unisat[idx_end_fine][0])
    duration = (end_date - start_date).value


    row_dict['Start'] = start_date.iso[:-4]
    row_dict['End'] = end_date.iso[:-4]
    row_dict['Duration [s]'] = duration * 86400
    row_dict['Time [JD]'] = np.copy(time_unisat[idx_start_fine[0]:idx_end_fine[0]]).to_value('jd')
    row_dict['Time relative [min]'] = (row_dict['Time [JD]'] - start_date.jd) * (60*24)
    row_dict['Min distance [km]'] = min(distance)
    row_dict['Distance [km]'] = distance[idx_start_fine[0]:idx_end_fine[0]]
    row_dict['Velocity [km/s]'] = velocity[idx_start_fine[0]:idx_end_fine[0]]
    row_dict['Acc sign'] = np.copy(acceleration_sign[idx_start_fine[0]:idx_end_fine[0]])
    row_dict['f [MHz]'] = f_observed(f0_unisat, row_dict['Velocity [km/s]'])
    row_dict['Deltaf [MHz]'] = f0_upmsat - row_dict['f [MHz]']
    row_dict['Min Deltaf [MHz]'] = min(row_dict['Deltaf [MHz]'])
    row_dict['Cross signal'] = np.any(np.abs(row_dict['Deltaf [MHz]'])<f_margin)

    access_list.append(row_dict)
    pbar.update(1)

pbar.close()
access_df = pd.DataFrame(data=access_list, index=ids)

case_id = 1

time = access_df.iloc[case_id]['Time relative [min]']
velocity = access_df.iloc[case_id]['Velocity [km/s]'] * access_df.iloc[case_id]['Acc sign']
f = access_df.iloc[case_id]['f [MHz]']
fs = f0_upmsat + np.array([1, -1]) * f_margin

plt.figure()
plt.plot(time, f)
plt.hlines(fs, min(time), max(time), colors='r')
plt.hlines(f0_upmsat, min(time), max(time), colors='r', linestyles='dotted')

plt.figure()
plt.plot(time, velocity)

plt.figure()
plt.plot(access_df['Cross signal'])

plt.figure()
plt.title('Min Deltaf [kHz]')
plt.plot(access_df['Min Deltaf [MHz]']*1000, 'o')

plt.figure()
plt.title('Min distance [km]')
plt.plot(access_df['Min distance [km]'], 'o')

plt.figure()
plt.title('Duration [s]')
plt.plot(access_df['Duration [s]'], 'o')
plt.show()


#sacar grafica con accercamientos, date vs min distance,  color distinto para
# los que ocurren sobre españa

# plt.figure()
# plt.scatter(time[filter_sats], distance[filter_sats])
# plt.vlines(events_time, min(distance[filter_sats]), max(distance[filter_sats]))
# plt.show()


# plt.figure()
# plt.plot(acceleration)
# # plt.show()

# plt.figure()
# plt.plot(filter_unisat)
# plt.plot(filter_upmsat)

# plt.figure()
# plt.plot(filter_sats)
# plt.show()

# print(acceleration.shape)

